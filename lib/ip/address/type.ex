defmodule IP.Address.Ecto.Type do
  use Ecto.Type
  @moduledoc """
  An IP Type for use with Ecto. Stores both IPv4 and IPv6 as a 128bit binary within the database.
  """

  def type, do: :binary

  def cast(%IP.Address{} = ip) do
    {:ok, ip}
  end

  def cast(ip_string) when is_bitstring(ip_string) do
    case IP.Address.from_string(ip_string) do
      {:ok, ip} -> {:ok, ip}
      {:error, msg} -> {:error, msg}
    end
  end

  def load(<<0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ipv4::binary>>) do
    IP.Address.from_binary(ipv4)
  end

  def load(ipv6 = <<_::unsigned-integer-size(128)>>) do
    IP.Address.from_binary(ipv6)
  end

  def dump(%IP.Address{} = ip) do
    integer_ip = IP.Address.to_integer(ip)
    {:ok, <<integer_ip::unsigned-integer-size(128)>>}
  end

  def dump(_), do: :error
end
